import React from 'react';
import logo from './gitlab.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://gitlab.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn GitLab CI
        </a>
      </header>
      <div className="participants">
        <h3>
          Add your name to the list to demonstrate you have completed the course.
        </h3>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Location</th>
              <th>Message</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Valentin</td>
              <td>@vdespa</td>
              <td>Romania 🇷🇴</td>
              <td>Thank you for taking this course.</td>
            </tr>
            <tr>
              <td>Ramazan</td>
              <td>@rmzturkmen</td>
              <td>Nederlands</td>
              <td>It was really a step-by-step, well-prepared and explained course. Thank you Valentin!.</td>
            </tr>
            <tr>
              <th>Adrian</th>
              <th>@popaadrianvd</th>
              <th>Romania 🇷🇴</th>
              <th>Thank you Valentin! This was a great tutorial, I know feel a lot more confident working with CI/CD pipelines.</th>
            </tr>
            <tr>
              <td>Fatih</td>
              <td>@fatihtepe</td>
              <td>Canada🇨🇦</td>
              <td>Thank you, Valentine</td>
            </tr>
            <tr>
              <td>Innocent Onyemaenu</td>
              <td>@rockcoolsaint</td>
              <td>Nigeria 🇳🇬</td>
              <td>Thank you for taking your time to create this wonderful and easy to follow along course.</td>
            </tr>
            <tr>
              <td>Muhammad Abutahir</td>
              <td>@abutahir</td>
              <td>India</td>
              <td>This is a must watch and implement course for every developer! Thank you so much 💙.</td>
            </tr>
            <tr>
              <td>KagemniKarimu</td>
              <td>@KagemniKarimu</td>
              <td>USA (Afrikan)</td>
              <td>Awesome course. I enjoyed it.</td>
            </tr>
            <tr>
              <td>Leo</td>
              <td>@leonel.barriga</td>
              <td>Argentina🇦🇷</td>
              <td>Thanks for the course Valentin. It was great!</td>
            </tr>
            <tr>
              <td>Phat NV</td>
              <td>@phat-nv</td>
              <td>Viet Nam</td>
              <td>Thank you for sharing this course.</td>
            </tr>
            <tr>
              <td>Rahul Gupta</td>
              <td>@rahulgupta141998</td>
              <td>India</td>
              <td>Thank you for the course</td>
            </tr>
            <tr>
              <td>Thomas Poth</td>
              <td>@thomas.poth</td>
              <td>Germany</td>
              <td>Valentin, you have made really great content. It fits perfectly as I currently have many GitLab projects running. One of the last secrets was GitLabc&apos;s CI/CD system. Thank you for this course!</td>
            </tr>
            <tr>
              <td>Santosh Dawanse</td>
              <td>@dawanse-santosh</td>
              <td>Nepal 🇳🇵</td>
              <td>Great content, Thank you for offering this course 💙.</td>
            </tr>
            <tr>
              <td>Axel Somerseth Cordova</td>
              <td>@axelsomerseth</td>
              <td>Honduras 🇭🇳</td>
              <td>Thank you for this course. I learned a lot! 🚀</td>
            </tr>
            <tr>
              <td>Eric Daly</td>
              <td>@linucksrox</td>
              <td>United States</td>
              <td>Thanks for this course, it helped me start making my own pipelines already.</td>
            </tr>
            <tr>
              <td>Maciej Friedel</td>
              <td>@ippolit</td>
              <td>Poland</td>
              <td>Thanks to this course I had a successful weekend</td>
            </tr>
            <tr>
              <td>Muiz Uvais</td>
              <td>@Muiz U</td>
              <td>Sri Lanka</td>
              <td>I found the course to be extremely useful, and informative. Thanks for making such great content</td>
            </tr>
            <tr>
              <td>Mohamed Nasrullah</td>
              <td>@nasr16</td>
              <td>India</td>
              <td>Really great course. Learned a lot in this one video. Thank you for offering this course 💙.</td>
            </tr>
            <tr>
              <td>Norman David</td>
              <td>@david_mbuvi</td>
              <td>Kenya</td>
              <td>I am really grateful for this course. Thank you for offering this course.</td>
            </tr>
            <tr>
              <td>Paul</td>
              <td>@phall659</td>
              <td>USA</td>
              <td>Great course! Thanks for taking time to put it together.</td>
            </tr>
            <tr>
              <td>Navi Singh</td>
              <td>@navisingh</td>
              <td>India</td>
              <td>I am very grateful. There are so many things to learn in this course for beginners like me. Superb instructor.</td>
            </tr>
            <tr>
              <td>Ezekiel Kolawole</td>
              <td>@eazybright</td>
              <td>Nigeria 🇳🇬</td>
              <td>I learnt alot from this course. Thanks so much!</td>
            </tr>
            <tr>
              <td>David</td>
              <td>@david1058</td>
              <td>Kenya 🇰🇪 / USA</td>
              <td>Alright Kubernetes, you&#39;re next!.</td>
            </tr>
            <tr>
              <td>Andrii Tugai</td>
              <td>@atugai</td>
              <td>Ukraine 🇺🇦</td>
              <td>Thank you for the course Valentin 😀 I learnt a lot</td>
            </tr>
            <tr>
              <th>Ebenezer Makinde</th>
              <th>@ebenezermakinde</th>
              <th>Nigeria 🇳🇬</th>
              <th>This course was just tailored to purpose. Great content. Thanks for this course</th>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default App;
